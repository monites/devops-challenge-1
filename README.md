# Devops Challange 1

This project is a light web server that returns a JSON response, when its / URL path is accessed:

```
{
  "timestamp": "<current date and time>",
  "hostname": "<Name of the host (IP also accepted)>",
  "engine": "<Name and/or version of the engine running>",
  "visitor ip": "<the IP address of the visitor>"
}
```

## Start Here

An Alpine Linux based tiny docker image, is intended to be used as minimal (old style) CGI server.

This image is based on Alpine Linux 3.3 and contains following packages. Image size is only 10 MB.

    mini_httpd
    bash
    curl

Bash scripting language is used to return such information from the container. The script is embedded in `index.html`

## Prerequisites

For this installation, you will need:

* Docker engine. If you're new to Docker, refer first to the [Getting Started](https://www.docker.com/get-started)
* kubectl
* Kubernetes. You can either use minikube in your local or kubernetes cluster in your favorite cloud provides such as AWS EKS, Azure AKS or Google GKE

## Build and Run in Docker

In your terminal, clone this repository.

`git clone https://bayramkaragoz@bitbucket.org/monites/devops-challenge-1.git`

* build docker image

```
cd devops-challenge-1/
docker build . -t mini-httpd-cgi
```

* Run docker image

`docker run -d -t -p 80:80 mini-httpd-cgi`

* call URL / from your local

`http://localhost/`

## Deploy and Access from Kubernetes

We assumed that you configured kubectl to access your kubernetes cluster
If not please check [this tutorial](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/).

* Apply manifest.yaml

```
cd devops-challenge-1/
kubectl apply -f manifest.yaml
```

* access from the following link if you have minikube in your local

`http://localhost:32456`

* access from below link if you have kubernetes cluster

`http://<public-node-ip>:<node-port>`

The service is configured as type:NodePort. You can change type as LoadBalancer if you want to access through public ip without indicating port number.
